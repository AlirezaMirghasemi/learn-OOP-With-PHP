<?php
/**
 * Created by PhpStorm.
 * User: alire
 * Date: 22/06/2017
 * Time: 09:18 PM
 */
require_once "./templates/header.php";
$userController = new App\Controller\UserController();
$userController->register();

?>


<!-- Page Content -->
<div class="container">

    <div class="row">
        <div class="col-lg-10 col-lg-offset-1">
          <?php
          if ($flash->hasMessages($flash::ERROR)) {
            $flash->display();
          }
          ?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Register Page</h3>
                </div>
                <div class="panel-body">
                    <form class="form-horizontal" method="post" action="register.php">
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">First Name</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="name" placeholder="Name"
                                       value="<?= old('name') ?>">
                            </div>

                        </div>
                        <div class="form-group">
                            <label for="family" class="col-sm-2 control-label">Last Name</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="family"
                                       placeholder="Last Name"
                                       value="<?= old('family') ?>">
                            </div>

                        </div>
                        <div class="form-group">
                            <label for="username" class="col-sm-2 control-label">Username</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="username"
                                       placeholder="Username"
                                       value="<?= old('username') ?>">
                            </div>

                        </div>
                        <div class="form-group">
                            <label for="email" class="col-sm-2 control-label">Email Address</label>
                            <div class="col-sm-10">
                                <input type="email" class="form-control" name="email"
                                       placeholder="Email"
                                       value="<?= old('email') ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="password" class="col-sm-2 control-label">Password</label>
                            <div class="col-sm-10">
                                <input type="password" class="form-control" name="password"
                                       placeholder="Password">
                            </div>

                        </div>
                        <div class="form-group">
                            <label for="confirm_password" class="col-sm-2 control-label">Confirm Password</label>
                            <div class="col-sm-10">
                                <input type="password" class="form-control" name="confirm_password"
                                       placeholder="Confirm Password">
                            </div>

                        </div>
                        <button type="submit" class="btn btn-success">Register</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- /.row -->

    <hr>
  <?php
  require_once "./templates/footer.php"
  ?>


