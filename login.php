<?php
/**
 * Created by PhpStorm.
 * User: alire
 * Date: 22/06/2017
 * Time: 09:18 PM
 */
require_once "./templates/header.php";
$userController = new App\Controller\UserController();
$userController->login();

?>


<!-- Page Content -->
<div class="container">

    <div class="row">
        <div class="col-lg-10 col-lg-offset-1">
          <?php
          if ($flash->hasMessages($flash::ERROR)) {
            $flash->display();
          }
          ?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Login Page</h3>
                </div>
                <div class="panel-body">
                    <form class="form-horizontal" method="post" action="login.php">
                        <div class="form-group">
                            <label for="email" class="col-sm-2 control-label">Email</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="email"
                                       placeholder="Email"
                                       value="<?= old('email') ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="password" class="col-sm-2 control-label">Password</label>
                            <div class="col-sm-10">
                                <input type="password" class="form-control" name="password"
                                       placeholder="Password">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember"> Remember me
                                    </label>
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-success">Login</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- /.row -->

    <hr>
  <?php
  require_once "./templates/footer.php"
  ?>


