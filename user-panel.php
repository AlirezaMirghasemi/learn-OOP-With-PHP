<?php
/**
 * Created by PhpStorm.
 * User: alire
 * Date: 22/06/2017
 * Time: 09:18 PM
 */
require_once "./templates/header.php";
if (!checkLogin())
    redirect('index.php');

?>


<!-- Page Content -->
<div class="container">

    <div class="row">
        <div class="col-lg-10 col-lg-offset-1">
           <h3>User Panel</h3>
        </div>
    </div>
    <!-- /.row -->

    <hr>
  <?php
  require_once "./templates/footer.php"
  ?>


