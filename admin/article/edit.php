<?php
/**
 * Created by PhpStorm.
 * User: alire
 * Date: 22/06/2017
 * Time: 09:18 PM
 */
require_once("./../../templates/header.php");
$article = (new \App\Controller\Admin\ArticleController())->edit();

?>


<!-- Page Content -->


<div class="container">
    <div class="row">
        <div class="col-lg-6 col-lg-offset-3">
            <h2>Edit Article</h2>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6 col-lg-offset-3">
          <?php
          if ($flash->hasMessages($flash::ERROR)) {
            $flash->display();
          }
          ?>
        </div>
    </div>
    <form class="form-horizontal" method="post"
          action="/learn-OOP-with-php/admin/article/update.php?id=<?= $article->id ?>">
        <div class="form-group">
            <label for="title" class="col-sm-2 control-label">Title</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="title"
                       placeholder="Title"
                       value="<?= $article->title ?>">
            </div>
        </div>
        <div class="form-group">
            <label for="body" class="col-sm-2 control-label">Body</label>
            <div class="col-sm-10">
                <textarea class="form-control" name="body"
                          placeholder="Body" rows="10"><?= $article->body ?></textarea>


            </div>
        </div>
        <button type="submit" class="btn btn-success">Edit</button>
    </form>
</div>

<hr>
<?php
require_once "./../../templates/footer.php"
?>


