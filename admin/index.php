<?php
/**
 * Created by PhpStorm.
 * User: alire
 * Date: 27/06/2017
 * Time: 01:40 PM
 */
require_once('./../templates/header.php');
$articles = (new App\Controller\Admin\AdminController())->index();
?>
    <div class="row">
        <div class="col-lg-8 col-lg-offset-2">
            <h3>Article List
                <a href="/learn-OOP-with-php/admin/article/create.php">
                    <button class="btn btn-xs btn-success">Create New Article</button>
                </a>
            </h3>
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Title</th>
                    <th></th>

                </tr>
                </thead>
                <tbody>
                <?php foreach ($articles as $article): ?>
                    <tr>
                        <th scope="row"><?= $article->id ?></th>
                        <td><?= $article->title ?></td>
                        <td>
                            <a href="/learn-OOP-with-php/admin/article/delete.php?id=<?= $article->id ?>">
                                <button type="button" class="btn btn-xs btn-danger">Delete</button>
                            </a>
                            <a href="/learn-OOP-with-php/admin/article/edit.php?id=<?= $article->id ?>">
                                <button type="button" class="btn btn-xs btn-warning">Edit</button>
                            </a>
                        </td>
                    </tr>

                <?php endforeach; ?>

                </tbody>
            </table>
        </div>
    </div>
<?php require_once('./../templates/header.php'); ?>