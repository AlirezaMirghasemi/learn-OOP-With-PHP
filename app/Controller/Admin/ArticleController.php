<?php
/**
 * Created by PhpStorm.
 * User: alire
 * Date: 27/06/2017
 * Time: 07:57 PM
 */

namespace App\Controller\Admin;

use App\Helper\Auth;
use App\Model\Article;
use Carbon\Carbon;

class ArticleController extends AdminController {
  public function create() {
    if (!request()->isPost()) {
      return;
    }
    $rules = [
      'title' => 'required',
      'body' => 'required'
    ];
    if (!$this->validation(request()->all(), $rules)) {
      return;
    }
    (new Article())->create([
      'title' => request('title'),
      'body' => request('body'),
      'user_id' => Auth::user()->id,
      'created_at' => Carbon::now()
    ]);
    redirect('/learn-OOP-with-php/admin');

  }

  public function delete() {
    if (empty(request('id')))
      redirect('/learn-OOP-with-php/admin');
    (new Article())->delete(request('id'));
    redirect('/learn-OOP-with-php/admin');

  }

  public function edit() {
    if (empty(request('id')))
      redirect('/learn-OOP-with-php/admin');
    return (new Article())->find('id', request('id'));
  }

  public function update($articleId) {
    $rules = [
      'title' => 'required',
      'body' => 'required'
    ];
    if (!$this->validation(request()->all(), $rules)) {
      return;
    }
    (new Article())->update($articleId,
      ['title' => request('title'),
        'body' => request('body'),]);
    redirect('/learn-OOP-with-php/admin');
  }
}
