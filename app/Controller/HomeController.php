<?php
/**
 * Created by PhpStorm.
 * User: alire
 * Date: 21/06/2017
 * Time: 09:02 PM
 */

namespace App\Controller;

use App\Model\Article;
use App\Model\DB;
use App\Model\Users;

class HomeController {
  public function index() {

    return (new Article())->all();
  }

  public function singleArticle() {
    if (!request('id'))
      redirect('index.php');
    return (new Article())->find('id', request('id'));
  }
}