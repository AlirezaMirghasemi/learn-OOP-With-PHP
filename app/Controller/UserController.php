<?php
/**
 * Created by PhpStorm.
 * User: alire
 * Date: 22/06/2017
 * Time: 09:24 PM
 */

namespace App\Controller;

use App\Helper\Auth;
use App\Model\Users;
use Carbon\Carbon;

class UserController extends Controller {
  public function __construct() {
    parent::__construct();
    if (checkLogin()) {
      redirect();
    }
  }

  public function register() {
    if (!request()->isPost()) {
      return;
    }
    $rules = [
      'name' => 'required',
      'family' => 'required',
      'username' => 'required|unique:users',
      'email' => 'required|email|unique:users',
      'password' => 'required|min:6|max:20',
      'confirm_password' => 'confirm:password'
    ];

    if (!$this->validation(request()->all(), $rules)) {
      return;
    }
    $user = new Users();
    $success = $user->create([
      'name' => request('name'),
      'family' => request('family'),
      'username' => request('username'),
      'email' => request('email'),
      'password' => password_hash(request('password'), PASSWORD_BCRYPT, ['const' => 12]),
      'created_at' => Carbon::now()
    ]);
    if ($success) {
      $this->flash->success("Welcome");
    } else {
      var_dump($success);
      die;
    }
    $this->flash->success("Welcome");
    redirect();
    return;
  }

  public function login() {
    if (!request()->isPost()) {
      return;
    }
    $rules = [
      'email' => 'required',
      'password' => 'required'
    ];

    if (!$this->validation(request()->all(), $rules)) {
      return;
    }

    $user = (new Users())->find('email', request('email'));
    if (!$user) {
      $this->flash->error('This Email Not Exist!');
      return;
    }
    $login = password_verify(request('password'), $user->password);
    if (!$login) {
      $this->flash->error('Email Or Password is Wrong!');
      return;
    }
    $remember = false;
    if (!empty(request('remember'))) {
      $remember = true;
    }
    Auth::login($user, $remember);
    redirect();
    return;
  }
}