<?php
/**
 * Created by PhpStorm.
 * User: alire
 * Date: 26/06/2017
 * Time: 05:28 PM
 */

namespace App\Contracts;


/**
 * Interface AuthInterface
 * @package App\Contracts
 */
interface AuthInterface {
  /**
   * @param $user
   * @param bool $remember
   * @return mixed
   */
  public static function login($user, $remember = false);

  /**
   * @return mixed
   */
  public static function check();

  /**
   * @return mixed
   */
  public static function logout();

  /**
   * @return mixed
   */
  public static function user();

}