<?php
/**
 * Created by PhpStorm.
 * User: alire
 * Date: 24/06/2017
 * Time: 10:16 AM
 */

namespace App\Contracts;


interface RequestInterface {
  /**
   * @param $field
   * @param bool $post
   * @return mixed
   */
  public function input($field, $post = true);

  /**
   * @param bool $post
   * @return mixed
   */
  public function all($post = true);

  /**
   * @return mixed
   */
  public function isPost();
}