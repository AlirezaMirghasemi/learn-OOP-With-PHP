<?php
/**
 * Created by PhpStorm.
 * User: alire
 * Date: 26/06/2017
 * Time: 06:30 PM
 */

namespace App\Contracts;


interface SessionInterface {
  /**
   * @param $key
   * @return mixed
   */
  public function exists($key);

  /**
   * @param $key
   * @return mixed
   */
  public function get($key);

  /**
   * @param $key
   * @param $value
   * @return mixed
   */
  public function set($key, $value);

  /**
   * @param $key
   * @return mixed
   */
  public function forget($key);
}