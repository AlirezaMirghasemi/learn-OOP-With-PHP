<?php
/**
 * Created by PhpStorm.
 * User: alire
 * Date: 26/06/2017
 * Time: 05:33 PM
 */

namespace App\Helper;


use App\Contracts\SessionInterface;

class Session implements SessionInterface {

  /**
   * @param $key
   * @return mixed
   */
  public function exists($key) {
    return array_key_exists($key, $_SESSION);
  }

  /**
   * @param $key
   * @return mixed
   */
  public function get($key) {
    return $this->exists($key) ? $_SESSION[$key] : false;
  }

  /**
   * @param $key
   * @param $value
   * @return mixed
   */
  public function set($key, $value) {
    $_SESSION[$key] = $value;
  }

  /**
   * @param $key
   * @return mixed
   */
  public function forget($key) {
    unset($_SESSION[$key]);
  }
}