<?php
/**
 * Created by PhpStorm.
 * User: alire
 * Date: 26/06/2017
 * Time: 05:33 PM
 */

namespace App\Helper;


use App\Contracts\SessionInterface;

class Cookie implements SessionInterface {

  /**
   * @param $key
   * @return mixed
   */
  public function exists($key) {
   array_key_exists($key,$_COOKIE) ;
  }

  /**
   * @param $key
   * @return mixed
   */
  public function get($key) {
    return $this->exists($key) ? $_COOKIE[$key] : false;
  }

  /**
   * @param $key
   * @param $value
   * @param string $time
   * @return mixed
   */
  public function set($key, $value, $time = '+30 day') {
    setcookie($key, $value, strtotime($time));
  }

  /**
   * @param $key
   * @return mixed
   */
  public function forget($key) {
    setcookie($key, '', strtotime('-5 day'),'/');
  }
}