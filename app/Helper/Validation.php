<?php
/**
 * Created by PhpStorm.
 * User: alire
 * Date: 22/06/2017
 * Time: 10:40 PM
 */

namespace App\Helper;
use \App\Model\DB;


/**
 * Class Validation
 * @package App\Helper
 */

class Validation {
  /**
   * @var
   */
  private $errors;
  /**
   * @var
   */
  private $data;

  /**
   * @param array $data
   * @param array $rules
   * @return bool
   */
  public function make(Array $data, Array $rules) {
    $valid = true;
    $this->data = $data;
    foreach ($rules as $item => $ruleset) {
      $ruleset = explode('|', $ruleset);
      foreach ($ruleset as $rule) {
        $pos = strpos($rule, ':');
        if ($pos !== false) {
          $parameter = substr($rule, $pos + 1);
          $rule = substr($rule, 0, $pos);
        } else {
          $parameter = "";
        }
        $MethodName = ucfirst($rule);
        $value = isset($data[$item]) ? $data[$item] : null;
        if (method_exists($this, $MethodName)) {
          if ($this->{$MethodName}($item, $value, $parameter) == false) {
            $valid = false;
            break;
          }
        }
      }
    }
    return $valid;
  }

  /**
   * @return mixed
   */
  public function getErrors() {
    return $this->errors;
  }

  /**
   * @param $item
   * @param $value
   * @return bool
   */
  private function required($item, $value) {
    if (strlen($value) == 0) {
      $this->errors[$item][] = "{$item} is required!";
      return false;
    }
    return true;
  }

  /**
   * @param $item
   * @param $value
   * @return bool
   */
  private function email($item, $value) {
    if (!filter_var($value, FILTER_VALIDATE_EMAIL)) {
      $this->errors[$item][] = "Email Address is Invalid!";
      return false;
    }
    return true;
  }

  /**
   * @param $item
   * @param $value
   * @param $parameter
   * @return bool
   */
  private function min($item, $value, $parameter) {
    if (strlen($value) < $parameter) {
      $this->errors[$item][] = "Minimum length of {$item} is {$parameter} Character! ";
      return false;
    }
    return true;
  }

  /**
   * @param $item
   * @param $value
   * @param $parameter
   * @return bool
   */
  private function max($item, $value, $parameter) {
    if (strlen($value) > $parameter) {
      $this->errors[$item][] = "Maximum length of {$item} is {$parameter} Character! ";
      return false;
    }
    return true;
  }

  /**
   * @param $item
   * @param $value
   * @param $parameter
   * @return bool
   */
  private function confirm($item, $value, $parameter) {
    $orginal = isset($this->data[$item]) ? $this->data[$item] : null;
    $confirm = isset($this->data[$parameter]) ? $this->data[$parameter] : null;
    if ($orginal !== $confirm) {
      $this->errors[$item][] = "The value of Confirm Password Most equal by Password!";
      return false;
    }
    return true;
  }

  private function unique($item, $value, $parameter) {
    $db = new DB();
    if (is_null($parameter)) {
      return false;
    }
    $db->from($parameter);
    if ($db->find($item, $value) != false) {
      $this->errors[$item][] = "$item is Already Exist!";
      return false;
    }
    return true;
  }
}