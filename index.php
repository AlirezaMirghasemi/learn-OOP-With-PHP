<?php
/**
 * Created by PhpStorm.
 * User: alire
 * Date: 21/06/2017
 * Time: 06:17 AM
 */
require_once "./templates/header.php";
$articles = (new \App\Controller\HomeController())->index();

?>


<!-- Page Content -->
<div class="container">

    <div class="row">

        <!-- Blog Entries Column -->
        <div class="col-md-8">

            <h1 class="page-header">
                Page Heading
                <small>Secondary Text</small>
            </h1>
          <?php foreach ($articles as $article) : ?>
              <!-- First Blog Post -->
              <h2>

                  <a href="article.php?id=<?= $article->id ?>">
                    <?= $article->title ?>
                  </a>
              </h2>
            <?php
            $article_user = (new \App\Model\Users())->find('id', $article->user_id);
            ?>
              <p class="lead">
                  by <a href="index.php"><?= $article_user->username ?></a>
              </p>
              <p><span class="glyphicon glyphicon-time"></span> Post
                  on<?= \Carbon\Carbon::parse($article->created_at); ?></p>
              <hr>
              <img class="img-responsive" src="http://placehold.it/900x300" alt="">
              <hr>
              <p><?= $article->body ?></p>
              <a class="btn btn-primary" href="article.php?id=<?= $article->id ?>">Read More <span
                          class="glyphicon glyphicon-chevron-right"></span></a>
              <hr>
          <?php endforeach; ?>
        </div>

        <!-- Blog Sidebar Widgets Column -->
        <div class="col-md-4">

            <!-- Blog Search Well -->
            <div class="well">
                <h4>Blog Search</h4>
                <div class="input-group">
                    <input type="text" class="form-control">
                    <span class="input-group-btn">
                            <button class="btn btn-default" type="button">
                                <span class="glyphicon glyphicon-search"></span>
                        </button>
                        </span>
                </div>
                <!-- /.input-group -->
            </div>
            =
            <!-- Side Widget Well -->
            <div class="well">
                <h4>Side Widget Well</h4>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore, perspiciatis adipisci accusamus
                    laudantium odit aliquam repellat tempore quos aspernatur vero.</p>
            </div>
        </div>
    </div>
    <!-- /.row -->
    <hr>
</div>
